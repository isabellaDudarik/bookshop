public class PublishHouse extends Information {
    String publishHouse;

    public PublishHouse(String name, int publishYear, String publishHouse) {
        super(name, publishYear);
        this.publishHouse = publishHouse;
    }

}
