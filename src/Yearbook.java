public class Yearbook extends PublishHouse {
    String subjects;

    public Yearbook(String name, int publishYear, String publishHouse, String subjects) {
        super(name, publishYear, publishHouse);
        this.subjects = subjects;
    }


    public String getInfoYearbook() {
        return String.format("Yearbook: %s, publishing yesr: %s, publishing House: %s, subjects: %s",
                name, publishYear, publishHouse, subjects);
    }

//    public String searchYear(int year) {
////        if (publishYear == year) {
////            System.out.println(getInfoYearbook());
////        }
////        return null;
////    }


    @Override
    public String searchYear(int year) {
        if (publishYear == year) {
        return getInfoYearbook();
    }return null;}

//    @Override
//    public String searchType(String type) {
//        return super.searchType(type);
//    }
}
