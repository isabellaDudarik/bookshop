import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter publish year: ");
        int year = Integer.parseInt(in.nextLine());


        Information[] information = BookShelf.generate();
        for (Information information1 : information) {
            System.out.println(information1.searchYear(year));
        }

        System.out.println("What do you want to take from the bookshelf? (book, magazine or yearbook)");
        String type = in.nextLine();

        if(type.equalsIgnoreCase("book")){
        for (Information information1 : information) {
            if (information1 instanceof Book) {
                System.out.println(String.format("Book: %s, publishing yesr: %d, publishing House: %s, author: %s",
                        information1.name, information1.publishYear, ((Book) information1).publishHouse, ((Book) information1).author));
            }}
        }
    }
}

