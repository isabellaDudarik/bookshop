public class Book extends PublishHouse {
    String author;

    public Book(String name, int publishYear, String publishHouse, String author) {
        super(name, publishYear, publishHouse);
        this.author = author;
    }

    public String getInfoBook() {
        return String.format("Book: %s, publishing yesr: %s, publishing House: %s, author: %s",
                name, publishYear, publishHouse, author);
    }


//    public String searchYear(int year) {
//        if (publishYear == year) {
//            System.out.println(getInfoBook());
//        }
//        return null;
//    }


    @Override
    public String searchYear(int year) {
        if (publishYear == year) {
        return getInfoBook();
    }  return null;}

//    @Override
//    public String searchType(String type) {
//        if(!type.equalsIgnoreCase("magazine")){
//            return getInfoBook();
//        }else return null;
//    }
}
