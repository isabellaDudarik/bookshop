import java.util.Arrays;

public class Magazine extends Information {
    String publishMonth;
    String subjects;


    public Magazine(String name, int publishYear, String publishMonth, String subjects
    ) {
        super(name, publishYear);
        this.publishMonth = publishMonth;
        this.subjects = subjects;
    }

    public String getInfoMagazine() {
        return String.format("Magazine: %s, publishing yesr: %s, publishing month: %s, subjects: %s",
                name, publishYear, publishMonth, subjects);
    }

//    public String searchYear(int year) {
//        if (publishYear == year) {
//            System.out.println(getInfoMagazine());
//        }
//        return null;
//    }


    @Override
    public String searchYear(int year) {
        if (publishYear == year) {
            return getInfoMagazine();
        }
        return null;
    }

//    @Override
//    public String searchType(String type) {
//        if(!type.equalsIgnoreCase("book")){
//            return getInfoMagazine();
//        }else return null;
//    }
}
