public class BookShelf {
    //    public static Book generate() {
//        return new Book("Harry Potter", 1997, "Bloomsbury", "Joanne Rowling");
//    }
//
//    public static Magazine generateMagazine() {
//        Subjects subjects = new Subjects("fashion");
//        return new Magazine("Vogue", 2019, "december", subjects);
//    }
//
//    public static Yearbook generateYearbook() {
//        Subjects subjects = new Subjects("picture");
//        return new Yearbook("A picture in time", 2019, "Kinsaung", subjects);
//    }
    public static Information[] generate() {
        Information informations[] = new Information[3];
        informations[0] = new Book("Harry Potter", 1997, "Bloomsbury", "Joanne Rowling");
        informations[1] = new Magazine("Vogue", 2019, "december", "fashion");
        informations[2] = new Yearbook("A picture in time", 2019, "Kinsaung", "picture");
        return informations;
    }

}
